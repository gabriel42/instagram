import React from "react";

import "./Modal.css";

class Modal extends React.Component {
  render() {
    return (
      <div className="modal__overlay">
        <button className="modal__close"><i className="fa fa-times"></i></button>
        <div className="modal__dialog">
          <div className="grid">
            <div className="column">
              <img className="modal__image" src="..." />
            </div>
            <div className="column modal__content">
              <header>
                <div className="avatar avatar--small">
                  <img src="..." />
                </div>
                <strong>...</strong>
                <span className="certification certification--small">
                  <i className="fa fa-check"></i>
                  <i className="fa fa-certificate"></i>
                </span>
                • 
                <strong className="follow">Follow</strong>
              </header>
              <aside>
                <p><strong>matteopasuto</strong> What a picture! Looks like some cool Nike Fitness commercials!</p>
                <p><strong>brian_whitfield</strong> The real mother of dragons</p>
              </aside>
              <footer className="modal__footer">
                <strong># likes</strong><br />
                <time>...</time>
                <form>
                  <input type="text" placeholder="Add a comment..." />
                </form>
              </footer>
            </div>
          </div>
        </div>
      </div>
    );
  }
};

export default Modal;
