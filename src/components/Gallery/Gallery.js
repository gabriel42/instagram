import React from "react";

import "./Gallery.css";

class Gallery extends React.Component {
  render() {
    return (
      <div className="container">
        <ul className="media">
          <li className="media__picture">
            <a href="/" >
              <img src="..." alt="" />
              <div className="media__overlay">
                <span><i className="fa fa-heart"></i> #</span>
                <span><i className="fa fa-comment"></i> #</span>
              </div>
            </a>
          </li>
        </ul>
      </div>
    );
  };
}

export default Gallery;
