# Instagram

## Context

Putting together in a basic web application concepts like: **Routing**, **APIs**, **PubSub Pattern**, and **Integrating External Libraries**.

## The Assignment

Build an _Instagram Profile Page_ clone, you already have all styles and components.

### Setup Instructions

In Terminal:

```sh
# (1) Clone the repo
$ git clone https://gitlab.com/agzeri/instagram

# (2) Go to directory
$ cd instagram

# (3) Install all dependencies
$ npm install

# (4) Open it in editor
$ atom .
```

## API Endpoints

All documentation from API is available here: [https://instagramapi.docs.stoplight.io/](https://instagramapi.docs.stoplight.io/).

## Deliverables

### Sprint 1 | Import Components

**Summary**

Import all needed components.

**Instructions**

+ Read carefully all JavaScript files.
+ Import the component called inside `render` method.

**Design**

![](/demos/s1.png)

## Instagram User Profile

### Sprint 2 | Implement Basic Routing

**Summary**

Configure routes in the web application using `react-router-dom`.

**Instructions**

+ Install the npm package (`react-router-dom`)
+ Import `BrowserRouter` from `react-router-dom` and wrap the App component. _This needs to happen in `index.js` file_.
+ Write the routes configuration in a separated file (`src/routes.js`)
  + Be sure you are not exporting a function, just export the JSX. (`export default (...)`).
  + Put all `Route` Components inside a `Switch` to create your configuration.

**Routes**

+ When I visit `/`, I should see the Instagram home.
+ When I visit `/@emrata` I should see the correct Instagram Profile page.
+ When I visit `/@some-user-that-not-exist` I should see a 404, Not Found.

**Notes**

You’ll need to declare two routes.

|#|Path|Type|Description|
|-|-|-|-|
|1|`/`|Static|This is your home|
|2|`/@emrata`|Dynamic|This is for every user that exists in our database|

##### For 404 Not Found route, you’ll need to validate if the user exists, then fetch information about that user, otherwise show the 404 Not Found component. THIS VALIDATION NEEDS TO BE DONE IN PAIR WITH FETCHING DATA (Sprint 3).


**Design**

![](/demos/s2.png)

### Sprint 3 | Fetch User Data

**Summary**

Consume the information from API and print in into DOM.

**Instructions**

+ You need to grab the username from URL to make the correct request (remember to delete `@`).
  + Use Component Lifecycle `componentDidMount` to fetch data
  + Take advantage from State and Props to save the information
+ There are two endpoints to consume data from user:
  + To get detailed user information (username, full name, verified account, posts, followers, following, etc.)
  + To extract all media (photos) information.
+ Remember to implement 404 Not Found route, you’ll need to read the body response from API and ask for a `status` property equal to 404. If it is true, you need to know what to do. 

**Design “User Profile”**

![](/demos/s3.png)

**Design “Not Found”**
![](/demos/s3_1.png)

## PubSub Pattern

### Sprint 4 | Modal Component

**Summary**

Implement modals in each photo, just like Instagram does.

**Instructions**

+ Install the library `pubsub-js`
+ Import in into the App component.
+ You need to declare your methods that will access to the State
  + Use `componentDidMount` to create them
  + Use `componentWillUnmount` to remove them from the stack
+ In the Gallery component import the library and connect with the correct method
+ You’ll need one method just to implement the close modal functionality.
  + Save in the state with a Boolean property if modal is open or not.

**Design**

![](/demos/s4.png)

### Sprint 5 | Fetch Photo Detail Information

**Summary**

Fetch information from clicked photo.

**Instructions**

+ Read API Docs to know which endpoint you need to get the correct information.
+ Grab the resource id from photo clicked.
+ Don’t forget to include also the comments attached to that media resource.

**Design**

![](/demos/s5.png)

### Sprint 6 | Post a Comment

**Summary**

Make a comment on a photo.

**Instructions**

+ Read **again** the API Docs to know which endpoint you need.
+ Once the comment has been created you’ll need to show it in the comments section from photo.
  + State is your best friend.

## External Libraries

### Sprint 7 | Implement Search Feature

**Summary**

This is one of the most exciting features to do, and fun. Every time you type a letter, the autocomplete should display all coincidences. To do this, use the package `react-autosuggest`.

**Instructions**

+ Install the `react-autosuggest` package
+ Read the documentation and understand what you need to do.

_HINT: Make a request to grab all users from our db and save them in our State. With the package we are going to find any coincidence from that State, avoiding requesting the API each time a user press a key._

**Design**

![](/demos/s7.png)

## Notes

- [PubSub Example](https://codesandbox.io/s/j44mljj2m9)
