import React from "react";

export default function Home() {
  return (
    <div className="center home">
      <p>Get the app.</p>
      <img alt="Available on the App Store" src="https://www.instagram.com/static/images/appstore-install-badges/badge_ios_english-en.png/180ae7a0bcf7.png" />
    </div>
  );
}
